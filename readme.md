# README

- Review code: [giasu-admin 5-2023](https://gitlab.com/gia-su-dong-nai/admin)

## TỔNG QUAN:

- Sử dụng stack không hợp lý cho 1 dự án nhỏ nhanh. Redux, Saga, ... chỉ nên sử dụng cho dự án lớn, cần quản lý state, side effect, (do copy từ project khác qua?)
- Tạo component riêng, để re-use không cần thiết, TỐN NHIỀU THỜI GIAN, khó đọc. Tần suất sử dụng lại không nhiều. Mỗi lần sử dụng lại vẫn phải truyền props riêng, sử dụng nhiều câu điều kiện.
- Copy code từ project khác qua, sửa sửa chạy cho xong, thu viện trùng lặp.
- Đặt tên biến khó hiểu, nhiều comments dư thừa
- Nhìn số thời gian commits không liên tục, cách NHIỀU NGÀY mới cập nhật repo 1 lần
- Nội dung commit không thể hiện được nội dung đã làm cho feature nào, chỉ thấy là update, fix, ... từng feature dở dang.

## Số lượng Commits, không tính phần merged.

**May 27, 2023**

- fix payload update tutor - Saul Vo authored
- schedule management - Saul Vo authored
- Update - letrunglong authored
- Update style table - letrunglong authored
- Salary attend

**May 25, 2023**

- Tutor to customer v1 - letrunglong authored

**May 24, 2023**

- Update filters - letrunglong authored
- Update filtes - letrunglong authored
- Update tutor ui- letrunglong authored

**May 19, 2023**

- Update Ver1 for customer - letrunglong authored
- tutor all function - letrunglong authored

**May 18, 2023**

- Tutor Dong nai - letrunglong authored
- Update CMS tutor v1 - letrunglong authored

**May 17, 2023**

- WIP: update crud - Saul Vo authored
- Update sider - letrunglong authored
- Package , study lession - letrunglong authored
- Update CMS - letrunglong authored
- Feature/resident - letrunglong authored

**May 04, 2023**

- Input Base, InputNumber, Modal right, Modal base, config style, resident page - letrunglong authored

**Apr 27, 2023**

- Button,Table, Text, config router , Resident Page - letrunglong authored

**Apr 25, 2023**

- Setup router, main page, theme - letrunglong authored

**Apr 24, 2023**

- add structure - Jimmy Chí Phan authored
- Keep calm and commit - Jimmy Chí Phan authored
- Initialize project using Create React App - Jimmy Chí Phan authored

## Stack đã sử dụng:

- Redux - Saga:
  - Redux: quản lý state
  - Saga: quản lý side effect
    Cấu trúc theo mô hình này không phù hợp với dự án nhỏ, chỉ phùh hợp dự án lớn nên cần có một cấu trúc phù hợp để dễ quản lý.
    Tốn thời gian để setup, cấu hình, tìm hiểu, debug, ...

![redux-saga](./images/redux-saga.png)

Chỉ riêng việc xây dựng cấu trúc thư mục như trên đã tốn không ít thời gian?

- mất bao lâu dể viết function onDeleteBaseActionRequest(..) này

- ![saga-watch-del](./images/saga-watch-del.png)
- Có chỗ comments như trong hình? Copy từ project khác qua, sửa sửa chạy cho xong? Những chức năng sau này, để đọc hiểu và sử dụng lại cho chuẩn cũng tốn nhiều thời gian.

## Components

- Button: Project này cơ bản, chỉ cần style, 1 sự kiện onClick, sử dụng. Không cần phải viết 1 component riêng sài ở tất mọi nơi như vậy. Có thể sử dụng thư viện Antd ... họ đang cung cấp sẵn, đủ dùng cho mọi trường hợp cơ bản.

![button](./images/button.png)

## Ví dụ về Kết hợp giữa Button và Redux, Action

```ts
const handleClose = useCallback(() => {
  setActionRecord(undefined);
}, []);

const handleRefresh = useCallback(() => {
  setReFresh((s) => !s);
}, []);
```

- Các hàm này dùng để làm gì vậy? biến không có ý nghĩa, tốn thời đọc hiểu, debug, ...

```tsx
return (
  <Col className="page-container apartment-page radius-4">
    {actionRecord?.action === EnumActionForm.EDIT ||
    actionRecord?.action === EnumActionForm.CREATE ? (
      <AddEdit
        record={actionRecord?.record}
        onRefresh={handleRefresh}
        onClose={handleClose}
        onActionClick={handleActionHeaderClick}
      />
    ) : (
      <Content onActionRecord={handleAction} isRefresh={isRefresh} />
    )}
    {actionRecord?.action === EnumActionForm.VIEW && (
      <Info open onClose={handleClose} />
    )}
    {actionRecord?.action === EnumActionForm.DELETE && (
      <Modal
      // ..
      >
        {/*...*/}
      </Modal>
    )}
  </Col>
);
```

- Lạm dụng re-use nên khi sử dụng có quá nhiều if, else, switch case, ... để xử lý các trường hợp khác nhau. Không dễ đọc, dễ hiểu, dễ debug, dễ bảo trì, dễ mở rộng, ...

- Viết re-use để sủ dụng chung cho đơn giản. Nhưng khi sử dụng, vẫn phải truyền props riêng, tạo thành nhiều tầng lớp.

- Chưa hoàn chỉnh dứt điểm từng feature, vẫn dùng fake-data hiển thị cho có trên UI

![fake-data](./images/salary-modal.png)

- INofity State Component: Không cần viết lại component này. Tần suất sử dụng lại không nhiều. Ví dụ, bao lâu thì sử dụng notify ở các ví trị khác nhau. Như ở trên, tránh viết lại 1 "thư viện" mới.

```tsx
position: {
  vertical: "top" | "bottom";
  horizontal: "left" | "center" | "right";
}
```

![notify](./images/inotify-state.png)

- Sao có "react-native-toast-message" ở hình dưới đây? Có phải đã copy từ 1 source mobile app khác sang, chỉnh sửa lại cho phù hợp với web app này không? Khi không hiểu và sử dụng code từ những project khác, sẽ gặp nhiều lỗi không mong muốn và tốn thời gian đọc hiểu.

![notify](./images/react-native-component.png)

- 2 thư viện 'dayjs', 'moment' đều làm việc với thời gian. Tại sao lại cần cùng lúc 2 thư viện này? Copy hàm sử dụng từ project khác qua nên phải mang theo thư viện đi kèm

![dayjs](./images/dayjs-moment.png)
